import React, { Component } from "react";
import { Dimensions, Text, View } from "react-native";

export default class splash extends Component {
  render() {
    return (
      <View
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "#222222"
        }}
      >
        <Text>Splashscreen</Text>
      </View>
    );
  }
}
