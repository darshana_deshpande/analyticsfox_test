import React, { Component } from "react";
import {
  Text,
  TextInput,
  View,
  Button,
  Alert,
  AsyncStorage
} from "react-native";
import styles from "./styles";

export default class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fname: "",
      lname: "",
      email: "",
      uname: "",
      password: ""
    };
  }

  componentDidMount() {
    this.getData();
  }
  getData = async () => {
    this.setState({
      fname: await AsyncStorage.getItem("fname"),
      lname: await AsyncStorage.getItem("lname"),
      email: await AsyncStorage.getItem("email"),
      uname: await AsyncStorage.getItem("uname"),
      password: await AsyncStorage.getItem("password")
    });
  };

  onPress = () => {
    console.log("onPRess");
    if (
      this.state.uname === null ||
      (this.state.uname === "" && this.state.pwd === null) ||
      this.state.pwd === ""
    ) {
      alert("Email and password cannot be empty");
    }
    console.log(this.state);

    AsyncStorage.setItem(
      "fname",
      this.state.fname !== null ? this.state.fname : ""
    );
    AsyncStorage.setItem(
      "lname",
      this.state.lname !== null ? this.state.lname : ""
    );
    AsyncStorage.setItem(
      "email",
      this.state.email !== null ? this.state.email : ""
    );
    AsyncStorage.setItem(
      "uname",
      this.state.uname !== null ? this.state.uname : ""
    );
    AsyncStorage.setItem(
      "password",
      this.state.password !== null ? this.state.password : ""
    );
    Alert.alert("Data Updated Successfully!");
  };

  render() {
    return (
      <View style={styles.rootContainer}>
        <Text style={{ fontSize: 20, marginBottom: 20 }}>EDIT PROFILE</Text>
        <TextInput
          style={styles.usernameInput}
          placeholder="First Name"
          onChangeText={fname => this.setState({ fname })}
          value={this.state.fname}
        />

        <TextInput
          style={styles.marginInput}
          placeholder="Last Name"
          onChangeText={lname => this.setState({ lname })}
          value={this.state.lname}
        />
        <TextInput
          style={styles.marginInput}
          placeholder="Email"
          onChangeText={email => this.setState({ email })}
          value={this.state.email}
        />
        <TextInput
          style={styles.marginInput}
          placeholder="Username"
          onChangeText={uname => this.setState({ uname })}
          value={this.state.uname}
        />
        <TextInput
          style={styles.marginInput}
          placeholder="Password"
          onChangeText={password => this.setState({ password })}
          value={this.state.password}
        />
        <Button title="UPDATE" onPress={this.onPress} />
        <View style={{ display: "flex", flexDirection: "row" }}>
          <Button
            title="LOGOUT"
            onPress={() => this.props.navigation.navigate("Login")}
          />
        </View>
      </View>
    );
  }
}
