import { StyleSheet, Dimensions } from "react-native";

export default (styles = StyleSheet.create({
  rootContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: Dimensions.get("screen").height
  },
  usernameInput: {
    height: 40,
    borderWidth: 1,
    borderColor: "#222222",
    width: "80%",
    borderRadius: 10
  },
  marginInput: {
    height: 40,
    borderWidth: 1,
    borderColor: "#222222",
    width: "80%",
    borderRadius: 10,
    marginTop: 15
  }
}));
