import React, { Component } from "react";
import {
  Text,
  TextInput,
  View,
  Button,
  Alert,
  AsyncStorage
} from "react-native";
import styles from "./styles";

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fname: "",
      lname: "",
      email: "",
      uname: "",
      password: ""
    };
  }

  onPress = () => {
    if (
      this.state.uname === "" ||
      this.state.pwd === "" ||
      this.state.fname === "" ||
      this.state.lname === "" ||
      this.state.email === ""
    ) {
      alert("Please input all the fields");
    } else {
      AsyncStorage.setItem(
        "fname",
        this.state.fname !== null ? this.state.fname : ""
      );
      AsyncStorage.setItem(
        "lname",
        this.state.lname !== null ? this.state.lname : ""
      );
      AsyncStorage.setItem(
        "email",
        this.state.email !== null ? this.state.email : ""
      );
      AsyncStorage.setItem(
        "uname",
        this.state.uname !== null ? this.state.uname : ""
      );
      AsyncStorage.setItem(
        "password",
        this.state.password !== null ? this.state.password : ""
      );
      Alert.alert("Registeration successful");
      this.props.navigation.navigate("Login");
    }
  };

  render() {
    return (
      <View style={styles.rootContainer}>
        <Text style={{ fontSize: 20, marginBottom: 20 }}>REGISTER</Text>
        <TextInput
          style={styles.usernameInput}
          placeholder="First Name"
          onChangeText={fname => this.setState({ fname })}
        />

        <TextInput
          style={styles.marginInput}
          placeholder="Last Name"
          onChangeText={lname => this.setState({ lname })}
        />
        <TextInput
          style={styles.marginInput}
          placeholder="Email"
          onChangeText={email => this.setState({ email })}
        />
        <TextInput
          style={styles.marginInput}
          placeholder="Username"
          onChangeText={uname => this.setState({ uname })}
        />
        <TextInput
          style={styles.marginInput}
          placeholder="Password"
          onChangeText={password => this.setState({ password })}
        />
        <Button title="Resigter" onPress={this.onPress} />
        <View style={{ display: "flex", flexDirection: "row" }}>
          <Text style={{ marginTop: 12 }}>Already a registered user?</Text>
          <Button
            title="LOGIN"
            onPress={() => this.props.navigation.navigate("LOGIN")}
          />
        </View>
      </View>
    );
  }
}
