import React, { Component } from "react";
import {
  Text,
  TextInput,
  View,
  Button,
  Alert,
  AsyncStorage,
  Dimensions
} from "react-native";
import styles from "./styles";

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uname: "",
      password: ""
    };
  }

  onPress = async () => {
    if (
      this.state.uname === null ||
      (this.state.uname === "" && this.state.pwd === null) ||
      this.state.pwd === ""
    ) {
      alert("Email and password cannot be empty");
    }

    const registered_uname = await AsyncStorage.getItem("uname");
    const registered_pwd = await AsyncStorage.getItem("password");
    if (
      this.state.uname === registered_uname &&
      this.state.password === registered_pwd
    ) {
      Alert.alert("Login successful!");
      this.props.navigation.navigate("EditProfile");
    } else {
      Alert.alert("Invalid username or password. Please try again.");
      this.state.uname = "";
      this.state.password = "";
    }
  };

  render() {
    return (
      <View style={styles.rootContainer}>
        <Text style={{ fontSize: 20, marginBottom: 20 }}>LOGIN</Text>
        <TextInput
          style={styles.usernameInput}
          placeholder="Username"
          onChangeText={uname => this.setState({ uname })}
        />

        <TextInput
          style={styles.marginInput}
          placeholder="Password"
          onChangeText={password => this.setState({ password })}
        />

        <Button title="LOGIN" onPress={this.onPress} />
        <View style={{ display: "flex", flexDirection: "row" }}>
          <Text style={{ marginTop: 12 }}>Not a registered user?</Text>
          <Button
            title="Register Now"
            onPress={() => this.props.navigation.navigate("Register")}
          />
        </View>
      </View>
    );
  }
}
